package com.shumov.tm.command.project;

import com.shumov.tm.command.AbstractCommand;

public class ProjectClearCommand extends AbstractCommand {

    public ProjectClearCommand() {

    }

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR PROJECTS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all projects]".toUpperCase());
        String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        serviceLocator.getProjectService().clearData(serviceLocator.getTaskService());
        System.out.println("[ALL PROJECTS HAVE BEEN DELETED SUCCESSFULLY]");
    }
}
