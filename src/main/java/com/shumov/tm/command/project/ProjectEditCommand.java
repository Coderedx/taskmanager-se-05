package com.shumov.tm.command.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.service.ProjectService;

public class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand() {

    }

    @Override
    public String command() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Edit project name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[EDIT PROJECT NAME BY ID]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdEdit = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectId(projectIdEdit);
        System.out.println("ENTER NEW PROJECT NAME:");
        String projectNameEdit = serviceLocator.getTerminalService().nextLine();
        projectService.isWrongProjectName(projectNameEdit);
        projectService.editProjectNameById(projectIdEdit,projectNameEdit);
        System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}
