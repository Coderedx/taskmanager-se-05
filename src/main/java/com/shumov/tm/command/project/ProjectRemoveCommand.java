package com.shumov.tm.command.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;

public class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand() {

    }

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdRemove = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        TaskService taskService = serviceLocator.getTaskService();
        projectService.isWrongProjectId(projectIdRemove);
        projectService.removeProjectById(taskService, projectIdRemove);
        System.out.println("PROJECT HAS BEEN REMOVED SUCCESSFULLY");
    }
}
