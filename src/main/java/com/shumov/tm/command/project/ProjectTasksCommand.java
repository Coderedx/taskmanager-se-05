package com.shumov.tm.command.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.service.ProjectService;

public class ProjectTasksCommand extends AbstractCommand {

    public ProjectTasksCommand() {

    }

    @Override
    public String command() {
        return "project-tasks";
    }

    @Override
    public String getDescription() {
        return "Review project tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REVIEW PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID:");
        String projectId = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectId(projectId);
        projectService.getProject(projectId);
        System.out.println("[TASK LIST]");
        for (Task task : serviceLocator.getTaskService().getProjectTasks(projectId)) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName());
        }
    }
}
