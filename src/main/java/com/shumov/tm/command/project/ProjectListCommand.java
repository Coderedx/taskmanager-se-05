package com.shumov.tm.command.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;

public class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand() {

    }

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        for (Project project : serviceLocator.getProjectService().getProjectList()) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName());
        }
    }
}
