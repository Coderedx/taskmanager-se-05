package com.shumov.tm.command.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.service.TaskService;

public class TaskListCommand extends AbstractCommand {

    public TaskListCommand() {

    }

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        for (Task task : serviceLocator.getTaskService().getTaskList()) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName()
                    + " PROJECT ID: "+ task.getIdProject());
        }
    }
}
