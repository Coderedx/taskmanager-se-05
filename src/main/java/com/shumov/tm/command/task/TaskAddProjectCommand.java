package com.shumov.tm.command.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;

public class TaskAddProjectCommand extends AbstractCommand {

    public TaskAddProjectCommand() {

    }

    @Override
    public String command() {
        return "task-add";
    }

    @Override
    public String getDescription() {
        return "Add task in project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ADD TASK IN PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        String projectIdAdd = serviceLocator.getTerminalService().nextLine();
        ProjectService projectService = serviceLocator.getProjectService();
        projectService.isWrongProjectId(projectIdAdd);
        projectService.getProject(projectIdAdd);
        System.out.println("ENTER TASK ID:");
        String taskIdAdd = serviceLocator.getTerminalService().nextLine();
        TaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongTaskId(taskIdAdd);
        Task task = taskService.getTask(taskIdAdd);
        task.setIdProject(projectIdAdd);
        taskService.addTaskInProject(taskIdAdd,task);
        System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
    }
}
