package com.shumov.tm.command.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.service.TaskService;

public class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand() {

    }

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        String taskIdRemove = serviceLocator.getTerminalService().nextLine();
        TaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongTaskId(taskIdRemove);
        taskService.removeTaskById(taskIdRemove);
        System.out.println("TASK HAS BEEN REMOVED SUCCESSFULLY");
    }
}
