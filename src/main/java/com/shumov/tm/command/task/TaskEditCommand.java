package com.shumov.tm.command.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.service.TaskService;

public class TaskEditCommand extends AbstractCommand {

    public TaskEditCommand() {

    }

    @Override
    public String command() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Edit task name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[EDIT TASK NAME BY ID]");
        System.out.println("ENTER TASK ID:");
        String taskIdEdit = serviceLocator.getTerminalService().nextLine();
        TaskService taskService = serviceLocator.getTaskService();
        taskService.isWrongTaskId(taskIdEdit);
        System.out.println("ENTER NEW TASK NAME:");
        String taskNameEdit = serviceLocator.getTerminalService().nextLine();
        taskService.isWrongTaskName(taskNameEdit);
        taskService.editTaskNameById(taskIdEdit,taskNameEdit);
        System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}
