package com.shumov.tm.command.task;

import com.shumov.tm.command.AbstractCommand;

public class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand() {

    }

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASKS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all projects]".toUpperCase());
        String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        serviceLocator.getTaskService().clearData();
        System.out.println("[ALL TASKS HAVE BEEN DELETED SUCCESSFULLY]");
    }
}
