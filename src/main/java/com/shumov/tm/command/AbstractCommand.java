package com.shumov.tm.command;

import com.shumov.tm.api.service.ServiceLocator;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public abstract String command();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
