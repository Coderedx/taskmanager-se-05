package com.shumov.tm.entity;



import java.util.*;

public class Project extends Entity {

    private String id = UUID.randomUUID().toString();
    private String name = "Default Name";
    private String description = "Default Description";
    private Date dateStart;
    private Date dateFinish;

    public Project(){

    }

    public Project(String name){
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id){ this.id = id; }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return id.equals(project.id) &&
                name.equals(project.name) &&
                description.equals(project.description) &&
                Objects.equals(dateStart, project.dateStart) &&
                Objects.equals(dateFinish, project.dateFinish);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, dateStart, dateFinish);
    }
}
