package com.shumov.tm.entity;

import java.util.*;

public class Task extends Entity {

    private String id = UUID.randomUUID().toString();
    private String name = "Default Name";
    private String description = "Default Description";
    private String idProject = "No Project";
    private Date dateStart;
    private Date dateFinish;

    public Task(){

    }

    public Task(String name, String idProject){
        this.name = name;
        this.idProject = idProject;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id){ this.id = id; }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id.equals(task.id) &&
                name.equals(task.name) &&
                description.equals(task.description) &&
                idProject.equals(task.idProject) &&
                Objects.equals(dateStart, task.dateStart) &&
                Objects.equals(dateFinish, task.dateFinish);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, idProject, dateStart, dateFinish);
    }
}
