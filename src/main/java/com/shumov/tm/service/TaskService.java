package com.shumov.tm.service;

import com.shumov.tm.entity.Task;
import com.shumov.tm.exception.input.WrongIdException;
import com.shumov.tm.exception.input.WrongInputException;
import com.shumov.tm.exception.input.WrongNameException;
import com.shumov.tm.exception.task.TaskException;
import com.shumov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService() {

    }

    public TaskService(TaskRepository taskRepository){
        this.taskRepository = taskRepository;
    }

    public void createTask(String taskName, String projectId) throws TaskException {
        taskRepository.persist(new Task(taskName, projectId));
    }

    public List<Task> getTaskList() throws TaskException {
        return taskRepository.findAll();
    }

    public Task getTask(String taskId) throws TaskException {
        return taskRepository.findOne(taskId);
    }

    public List<Task> getProjectTasks(String projectId) throws TaskException {
        List<Task> tasks = new ArrayList<>();
        for (Task task : taskRepository.findAll()) {
            if(projectId.equals(task.getIdProject())){
                tasks.add(task);
            }
        }
        return tasks;
    }

//    public void mergeTask(String taskId, Task task) throws TaskException{
//        taskRepository.merge(taskId,task);
//    }

    public void clearData() throws TaskException {
        taskRepository.removeAll();
    }

    public void editTaskNameById(String taskId, String taskName) throws  TaskException {
        Task task = taskRepository.findOne(taskId);
        task.setName(taskName);
        taskRepository.merge(taskId, task);
    }

    public void removeTaskById(String taskId) throws TaskException{
        taskRepository.remove(taskId);
    }

    public void addTaskInProject(String taskId, Task task) throws TaskException{
        taskRepository.merge(taskId,task);
    }

    public void isWrongTaskName(String taskName) throws WrongInputException {
        if (taskName==null || taskName.isEmpty()){
            throw new WrongNameException();
        }
    }

    public void isWrongTaskId(String taskId) throws WrongInputException{
        if(taskId==null || taskId.isEmpty()){
            throw new WrongIdException();
        }
    }
}
