package com.shumov.tm.api.service;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;

import java.util.List;
import java.util.Scanner;

public interface ServiceLocator {

    TaskService getTaskService();
    ProjectService getProjectService();
    Scanner getTerminalService();
    List<AbstractCommand> getCommands();
}
