package com.shumov.tm.exception.project;

public class ProjectIsAlreadyExistException extends ProjectException {

    public ProjectIsAlreadyExistException(){
        super("PROJECT WITH THIS ID IS ALREADY EXIST!");
    }
}
