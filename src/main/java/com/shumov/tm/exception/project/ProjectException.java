package com.shumov.tm.exception.project;

public class ProjectException extends Exception {

    public ProjectException() {

    }

    public ProjectException(String message) {
        super(message);
    }

}
