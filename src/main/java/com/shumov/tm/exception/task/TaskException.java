package com.shumov.tm.exception.task;

public class TaskException extends Exception {

    public TaskException() {
    }

    public TaskException(String message) {
        super(message);
    }
}
