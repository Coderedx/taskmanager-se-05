package com.shumov.tm.exception.input;

public class WrongIdException extends WrongInputException {

    public WrongIdException(){
        super("WRONG ID!");
    }
}
