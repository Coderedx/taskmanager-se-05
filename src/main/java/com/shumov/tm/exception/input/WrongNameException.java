package com.shumov.tm.exception.input;

public class WrongNameException extends WrongInputException {

    public WrongNameException() {
        super("WRONG NAME!");
    }
}
