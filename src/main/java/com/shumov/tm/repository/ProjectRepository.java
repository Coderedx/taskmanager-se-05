package com.shumov.tm.repository;

import com.shumov.tm.entity.Project;
import com.shumov.tm.exception.project.ProjectException;
import com.shumov.tm.exception.project.ProjectIsAlreadyExistException;
import com.shumov.tm.exception.project.ProjectListIsEmptyException;
import com.shumov.tm.exception.project.ProjectNotExistException;


import java.util.*;

public class ProjectRepository {

    private Map<String, Project> projectMap = new LinkedHashMap<>();

    public ProjectRepository(){

    }

    public List<Project> findAll() throws ProjectException {
        if(projectMap.isEmpty()) {
            throw new ProjectListIsEmptyException();
        } else {
            return new ArrayList<>(projectMap.values());
        }
    }

    public Project findOne(String id) throws ProjectException {
        if(projectMap.isEmpty()){
            throw new ProjectListIsEmptyException();
        } else if(!projectMap.containsKey(id)){
            throw new ProjectNotExistException();
        } else {
            return projectMap.get(id);
        }

    }

    public void persist(Project project) throws ProjectException {
        if(projectMap.containsKey(project.getId())){
            throw new ProjectIsAlreadyExistException();
        } else {
            projectMap.put(project.getId(), project);
        }

    }

    public void merge(String id, Project project) {
        project.setId(id);
        projectMap.put(id, project);
    }

    public void remove(String id) throws ProjectException {
        if(projectMap.isEmpty()){
            throw new ProjectListIsEmptyException();
        } else if(!projectMap.containsKey(id)){
            throw new ProjectNotExistException();
        } else {
            projectMap.remove(id);
        }
    }

    public void removeAll() throws ProjectException {
        if(projectMap.isEmpty()){
            throw new ProjectListIsEmptyException();
        } else {
            projectMap.clear();
        }
    }

}
