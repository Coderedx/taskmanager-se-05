package com.shumov.tm.bootstrap;


import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.command.help.ExitCommand;
import com.shumov.tm.command.help.HelpCommand;
import com.shumov.tm.command.project.*;
import com.shumov.tm.command.task.*;
import com.shumov.tm.exception.command.CommandCorruptException;
import com.shumov.tm.exception.command.CommandWrongException;
import com.shumov.tm.repository.ProjectRepository;
import com.shumov.tm.repository.TaskRepository;
import com.shumov.tm.service.ProjectService;
import com.shumov.tm.service.TaskService;

import java.util.*;

public class Bootstrap implements ServiceLocator {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private ProjectService projectService = new ProjectService(new ProjectRepository());
    private TaskService taskService = new TaskService(new TaskRepository());
    private Scanner terminalService = new Scanner(System.in);

    public Bootstrap() {

    }

    public void init() {
        AbstractCommand[] commandsInit = {new HelpCommand(), new ProjectClearCommand(),
                new ProjectCreateCommand(), new ProjectEditCommand(), new ProjectListCommand(),
                new ProjectRemoveCommand(), new ProjectTasksCommand(), new TaskAddProjectCommand(),
                new TaskClearCommand(), new TaskCreateCommand(), new TaskEditCommand(), new TaskListCommand(),
                new TaskRemoveCommand(), new ExitCommand()};
        commands.clear();
        for (AbstractCommand abstractCommand : commandsInit) {
            try {
                registry(abstractCommand);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        start();
    }

    private void registry(final AbstractCommand command) throws CommandCorruptException {
        final String cliCommand = command.command();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty()) {
            throw new CommandCorruptException();
        }
        if (cliDescription == null || cliDescription.isEmpty()) {
            throw new CommandCorruptException();
        }
        command.setServiceLocator(this);
        commands.put(command.command(), command);
    }

    private void start() {
        System.out.println("[WELCOME TO TASK MANAGER]");
        System.out.println("[ENTER \"help\" TO GET COMMAND LIST]");
        String command = "";
        while (!"exit".equals(command)) {
            System.out.println("\nENTER COMMAND:");
            command = terminalService.nextLine();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            throw new CommandWrongException();
        }
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            throw new CommandWrongException();
        }
        abstractCommand.execute();
    }

    public Scanner getTerminalService() {
        return terminalService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }
}
