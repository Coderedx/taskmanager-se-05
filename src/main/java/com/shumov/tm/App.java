package com.shumov.tm;

import com.shumov.tm.bootstrap.Bootstrap;

public class App
{
    public static void main( String[] args )
    {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
